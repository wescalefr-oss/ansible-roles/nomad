---
- name: Install mandatory packages for installation
  package:
    name:
      - unzip
      - gnupg
      - perl-Digest-SHA
    state: present

- name: Create system group
  group:
    name: "{{ nomad_user_name }}"
    gid: "{{ nomad_user_id }}"
    system: yes

- name: Create system user
  user:
    name: "{{ nomad_user_name }}"
    groups: "{{ nomad_user_add_groups + [nomad_user_name] }}"
    uid: "{{ nomad_user_id }}"
    home: "{{ nomad_config_dir }}"
    shell: "/bin/false"
    createhome: no
    system: yes

- name: Create config directory
  file:
    path: "{{ nomad_config_dir }}"
    owner: "{{ nomad_user_name }}"
    group: "{{ nomad_user_name }}"
    state: directory
    mode: 0750

- name: Create cache directory for download
  file:
    path: "{{ nomad_ansible_cache_dir }}"
    state: directory
    mode: 0750

- name: Copy Hashicorp public gpg key
  copy:
    src: "hashicorp.asc"
    dest: "{{ nomad_ansible_cache_dir }}/hashicorp.asc"
  notify: Import Hashicorp public gpg key

- meta: flush_handlers

- name: Download nomad {{ nomad_version }} sha256sums
  get_url:
    url: "{{ nomad_sha256sums_url }}"
    dest: "{{ nomad_sha256sums_dest }}"
  notify: Verify nomad sha256sums signature

- name: Download nomad {{ nomad_version }} sha256sums signature
  get_url:
    url: "{{ nomad_sha256sums_sig_url }}"
    dest: "{{ nomad_sha256sums_sig_dest }}"
  notify: Verify nomad sha256sums signature

- meta: flush_handlers

- name: Download nomad {{ nomad_version }} release
  get_url:
    url: "{{ nomad_archive_url }}"
    dest: "{{ nomad_archive_dest }}"
  notify: Verify nomad release hash

- meta: flush_handlers

- name: Unarchive nomad {{ nomad_version }} release
  unarchive:
    src: "{{ nomad_archive_dest }}"
    dest: "{{ nomad_bin_dir }}"
    creates: "{{ nomad_bin_path }}"
    remote_src: yes

- name: Set rights on nomad binary
  file:
    path: "{{ nomad_bin_path }}"
    owner: "root"
    group: "{{ nomad_user_name }}"
    mode: 0750

- name: Create nomad data directory
  file:
    path: "{{ nomad_data_dir }}"
    state: directory
    mode: 0750
    owner: "{{ nomad_user_name }}"
    group: "{{ nomad_user_name }}"

- name: Create nomad system service
  template:
    src: "nomad.service.j2"
    dest: "/lib/systemd/system/nomad.service"
    mode: 0640
  notify: Restart nomad service

- name: Create rsyslog configuration
  copy:
    dest: "/etc/rsyslog.d/nomad.conf"
    mode: 0640
    content: |-
      :programname, isequal, "nomad" {{ nomad_log_dest }}
      & stop
  notify: Restart rsyslog service

- name: Create local facts
  copy:
    dest: "/etc/ansible/facts.d/nomad.fact"
    content: >-
      {
        "data_dir": "{{ nomad_data_dir }}",
        "config_dir": "{{ nomad_config_dir }}",
        "version": "{{ nomad_version }}",
        "log_dest": "{{ nomad_log_dest }}"
      }
